// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2009 - DIGITEO - Pierre MARECHAL <pierre.marechal@scilab.org>
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// <-- ATOMS MANDATORY -->
// <-- CLI SHELL MODE -->

this_toolbox_name = "toolbox_4";
atomsLoad(this_toolbox_name);
this_toolbox_path = atomsGetLoadedPath(this_toolbox_name);
t4_common         = pathconvert(this_toolbox_path+"/tests/unit_tests/t4_common.txt",%F);

ref = [ "Toolbox 4 -> Function 1" ; "Toolbox 2 -> Function 1" ; "Toolbox 1 -> Function 1" ];
if or( t4_function1() <> ref ) then pause, end
if isempty(fileinfo(t4_common)) then pause, end
t4_common_mat = mgetl(t4_common);
if t4_function1()<>t4_common_mat(1:3) then pause, end
