main_dir = get_absolute_file_path("build_all.sce");

exec(main_dir + "tbx.sce");

for kn=1:size(tbx_names, "*")
    for kv=1:size(tbx_versions(kn), "*")
        bpath = main_dir + tbx_names(kn) + "/" + tbx_versions(kn)(kv) + "/builder.sce";
        exec(bpath);
    end
end

exit