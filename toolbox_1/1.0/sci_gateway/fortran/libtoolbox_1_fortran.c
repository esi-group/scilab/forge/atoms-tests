#include <mex.h> 
#include <sci_gateway.h>
static int direct_gateway(char *fname,void F(void)) { F();return 0;};
extern Gatefunc sci_t1_function6;
static GenericTable Tab[]={
  {(Myinterfun)sci_gateway,sci_t1_function6,"t1_function6"},
};
 
int C2F(libtoolbox_1_fortran)()
{
  Rhs = Max(0, Rhs);
  (*(Tab[Fin-1].f))(Tab[Fin-1].name,Tab[Fin-1].F);
  return 0;
}
