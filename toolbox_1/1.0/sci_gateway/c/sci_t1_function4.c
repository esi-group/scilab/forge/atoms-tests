#include <stdlib.h>
#include "api_scilab.h"
#include "Scierror.h"

extern char *t1_function4(void);

int sci_t1_function4(char *fname, void* pvApiCtx)
{  
  int m_out = 1, n_out = 1;
  char **OutputString = NULL;
  char *result = t1_function4();

  CheckRhs(0,0) ;
  CheckLhs(1,1) ;
  
  OutputString = (char**)malloc(sizeof(char*)*1);
  if (OutputString)
  {
  	OutputString[0] = result;
  	createMatrixOfString(pvApiCtx, Rhs + 1, m_out, n_out, OutputString);
  	LhsVar(1) = Rhs + 1; 
  	free(OutputString);
  	OutputString = NULL;
  }
  else
  {
  	Scierror(999,"%s: memory allocation error.\n",fname);
  }
  return 0;
}
