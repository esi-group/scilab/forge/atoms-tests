// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2009 - DIGITEO - Pierre MARECHAL <pierre.marechal@scilab.org>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


mode(-1);
lines(0);

function toolbox1BuildToolbox()
    TOOLBOX_NAME  = "toolbox_1";
    TOOLBOX_TITLE = "Toolbox Test Number 1";

    toolbox_dir = get_absolute_file_path("builder.sce");

    tbx_builder_macros(toolbox_dir);
    tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
    tbx_builder_help(toolbox_dir);
endfunction

toolbox1BuildToolbox();
clear toolbox1BuildToolbox;

