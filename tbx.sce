tbx_names = [];
tbx_versions = list();

tbx_names(1) = "toolbox_1";
tbx_versions(1) = ["1.0" "2.0"];

tbx_names(2) = "toolbox_2";
tbx_versions(2) = ["1.0" "2.0" "2.1"];

tbx_names(3) = "toolbox_3";
tbx_versions(3) = ["1.0"];

tbx_names(4) = "toolbox_4";
tbx_versions(4) = ["1.0" "1.1"];

tbx_names(5) = "toolbox_5";
tbx_versions(5) = ["1.0" "1.1" "1.2" "2.0"];

tbx_names(6) = "toolbox_6";
tbx_versions(6) = ["1.0"];
